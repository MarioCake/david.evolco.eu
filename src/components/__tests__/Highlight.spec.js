import Highlight from '../Highlight.vue'
import {shallowMount} from '@vue/test-utils'

describe('dom rendering', () => {
    test('slot displays value correctly', () => {
        const wrapper = shallowMount(Highlight, {
            slots: {
                default: '2016'
            }
        });
        expect(wrapper.text()).toBe('2016');
    });

    test('text is empty with no slot specified', () => {
        const wrapper = shallowMount(Highlight);

        expect(wrapper.text()).toBe('');
    });

    test('evaluates node', () => {
        const wrapper = shallowMount(Highlight, {
            slots: {
                default: '<div class="test">hello</div>'
            }
        });

        expect(wrapper.contains('div.test')).toBeTruthy();
        expect(wrapper.find('div.test').text()).toBe('hello');
    });

    test('has default colors', () => {
        const wrapper = shallowMount(Highlight);

        expect(wrapper.element.style.backgroundColor).toBe('rgba(32, 32, 32, 0.5)');
        expect(wrapper.element.style.color).toBe('white');
    });

    test('accepts other background color', () => {
        const wrapper = shallowMount(Highlight, {
            propsData: {
                backgroundColor: 'red'
            }
        });

        expect(wrapper.element.style.backgroundColor).toBe('red');
    });

    test('accepts other foreground color', () => {
        const wrapper = shallowMount(Highlight, {
            propsData: {
                color: 'blue'
            }
        });

        expect(wrapper.element.style.color).toBe('blue');
    });

    test('renders everything', () => {
        const wrapper = shallowMount(Highlight, {
            propsData: {
                color: 'blue'
            },
            slots: {
                default: 'Text to highlight'
            }
        });
        expect(wrapper.html()).toMatchSnapshot();
    })
});
