import Vuex from "vuex";
import Project from "../Project/Project";
import {mount, shallowMount} from "@vue/test-utils";
import {iff} from '../../../tests/iff'


describe('Project.vue', () => {
    let props;
    let storeOptions;

    function reset(){
        props = {};
        storeOptions = {
            getters: {
                "sprintAddPossible": () => true,
                "projectEditPossible": () => true,
                "projectDeletePossible": () => true,
            }
        }
    }

    function projectIsNotSpecified(){
        delete props.project;
    }

    function projectIsSpecified(){
        projectIsSpecified.amountOfSpecifiedSprints = projectIsSpecified.amountOfSpecifiedSprints || 0;
        // Need to get the argument like that because otherwise the beforeEach(...) call would pass a callback function
        // https://github.com/facebook/jest/issues/8313#issuecomment-482294236
        const project = arguments[0];
        if (typeof project !== 'undefined' && project !== null) {
            const obj = {
                ['projectIsSpecifiedWithData_(' + (++projectIsSpecified.amountOfSpecifiedSprints) + ')']: function () {
                    props.project = project;
                }
            };
            return obj['projectIsSpecifiedWithData_(' + projectIsSpecified.amountOfSpecifiedSprints + ')'];
        }
        props.project = {
            id: 1,
            projectName: 'Mock project title',
            sprints: [{}, {}, {}]
        };
    }

    async function addSprintEventShouldBeEmittedOnButtonClick(projectComponent) {
        const addSprintButton = projectComponent.find('div.addSprint');
        expect(addSprintButton.isEmpty.bind(addSprintButton)).not.toThrowError();

        addSprintButton.trigger('click');
        await projectComponent.vm.$nextTick();
        expect(projectComponent.emitted('addSprint')).toHaveLength(1);
    }

    async function editProjectEventShouldBeEmittedOnButtonClick(projectComponent) {
        const editProjectButton = projectComponent.find('div.editProject');
        expect(editProjectButton.isEmpty.bind(editProjectButton)).not.toThrowError();

        editProjectButton.trigger('click');
        await projectComponent.vm.$nextTick();
        expect(projectComponent.emitted('editProject')).toHaveLength(1);
    }

    async function deleteProjectEventShouldBeEmittedOnButtonClick(projectComponent) {
        const deleteProjectButton = projectComponent.find('div.deleteProject');
        expect(deleteProjectButton.isEmpty.bind(deleteProjectButton)).not.toThrowError();

        deleteProjectButton.trigger('click');
        await projectComponent.vm.$nextTick();
        expect(projectComponent.emitted('deleteProject')).toHaveLength(1);
    }

    function shouldShowPropertyIsNotDefinedError(consoleSpy){
        expect(shallowMountProject).not.toThrowError();
        expect(consoleSpy).toBeCalledWith(expect.stringContaining('[Vue warn]: Missing required prop: "project"'));
    }

    function shouldNotGiveAnyErrors(consoleSpy){
        expect(shallowMountProject).not.toThrowError();
        expect(consoleSpy).not.toBeCalled();
    }

    function shouldRenderEverything(projectComponent){
        expect(projectComponent.html()).toMatchSnapshot();
    }

    function shallowMountProject(){
        const store = new Vuex.Store(storeOptions);
        return shallowMount(Project, {propsData: props, store});
    }

    function mountProject(){
        const store = new Vuex.Store(storeOptions);
        return mount(Project, {propsData: props, store});
    }

    iff.reset = reset;

    iff.injectParameter = () => {
        return shallowMountProject();
    };

    iff(projectIsSpecified).then(shouldRenderEverything);

    iff.injectParameter = () => {
        return mountProject();
    };
    iff(projectIsSpecified({
        projectName: 'My mock project name',
        sprints: [],
    })).then(shouldRenderEverything)
        .and(addSprintEventShouldBeEmittedOnButtonClick)
        .and(editProjectEventShouldBeEmittedOnButtonClick)
        .and(deleteProjectEventShouldBeEmittedOnButtonClick);

    iff.injectParameter = () => {
        const consoleSpy = jest.spyOn(console, 'error');
        consoleSpy.mockImplementation(() => true);
        consoleSpy.mockClear();
        return consoleSpy;
    };

    iff(projectIsSpecified).then(shouldNotGiveAnyErrors);
    iff(projectIsNotSpecified).then(shouldShowPropertyIsNotDefinedError);


    iff.startTests();
});