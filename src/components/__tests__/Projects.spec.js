import Projects from '../../views/Projects';
import Vuex from 'vuex';
import {shallowMount} from "@vue/test-utils";
import {iff} from '../../../tests/iff'
import getStoreOptions from '../../../tests/storeMock'
import Project from "../Project/Project";
import store from "../../store/store";

describe('Projects.vue', () => {
    let storeOptions;

    function threeProjectsAreAvailable() {
        storeOptions.state.projects.projectsData = [{}, {}, {}];
    }

    function projectsAreNotAvailable() {
        storeOptions.state.projects.projectsData = [];
    }

    function projectsAreLoading() {
        storeOptions.state.projects.loadingProjects = true;
    }

    function projectsAreNotLoading() {
        storeOptions.state.projects.loadingProjects = false;
    }

    function shouldRenderLoadingProjects(wrapper) {
        expect(wrapper.text()).toContain('Loading projects...');
    }
    function shouldNotRenderLoadingProjects(wrapper) {
        expect(wrapper.text()).not.toContain('Loading projects...');
    }

    function shouldRenderNoProjectsAvailable(wrapper){
        expect(wrapper.text()).toContain('No projects available');
    }

    function shouldNotRenderNoProjectsAvailable(wrapper){
        expect(wrapper.text()).not.toContain('No projects available');
    }

    function shouldRender3Projects(wrapper){
        expect(wrapper.findAll(Project)).toHaveLength(3);
    }
    function shouldRenderNoProjects(wrapper){
        expect(wrapper.findAll(Project)).toHaveLength(0);
    }

    iff.reset = function(){
        storeOptions = getStoreOptions();
    };
    iff.injectParameter = () => {
        const store = new Vuex.Store(storeOptions);
        return shallowMount(Projects, {store});
    };

    iff(projectsAreNotAvailable).and(projectsAreLoading)
        .then(shouldRenderLoadingProjects)
        .and(shouldNotRenderNoProjectsAvailable)
        .and(shouldRenderNoProjects);

    iff(projectsAreNotAvailable).and(projectsAreNotLoading)
        .then(shouldNotRenderLoadingProjects)
        .and(shouldRenderNoProjectsAvailable)
        .and(shouldRenderNoProjects);

    iff(threeProjectsAreAvailable).and(projectsAreLoading)
        .then(shouldNotRenderLoadingProjects)
        .and(shouldNotRenderNoProjectsAvailable)
        .and(shouldRender3Projects);

    iff(threeProjectsAreAvailable).and(projectsAreNotLoading)
        .then(shouldNotRenderLoadingProjects)
        .and(shouldNotRenderNoProjectsAvailable)
        .and(shouldRender3Projects);

    iff.injectParameter = function(){
        return shallowMount(Projects, {store});
    };


    iff.startTests();

    // describe('if projects are loading', () => {
    //     function reset() {
    //         resetStore();
    //         storeOptions.state.projects.loadingProjects = true;
    //     }
    //
    //     beforeAll(reset);
    //
    //     describe('if projects are available', () => {
    //         let wrapper;
    //         const projects = [{}, {}, {}];
    //
    //         beforeAll(function () {
    //             reset();
    //             storeOptions.state.projects.projectsData = projects;
    //
    //             const store = new Vuex.Store(storeOptions);
    //             wrapper = shallowMount(Projects, {store, localVue,});
    //         });
    //
    //         test('should render all the projects', () => {
    //             expect(wrapper.findAll(Project)).toHaveLength(projects.length);
    //         });
    //
    //         test('should not show "No projects available"', () => {
    //             expect(wrapper.text()).not.toContain('No projects available');
    //         });
    //
    //         test('should not show "Loading projects..."', () => {
    //             expect(wrapper.text()).not.toContain('Loading projects...');
    //         });
    //     });
    //
    //     describe('if projects are not available', () => {
    //         let wrapper;
    //
    //         beforeAll(function () {
    //             reset();
    //             storeOptions.state.projects.projectsData = [];
    //
    //             const store = new Vuex.Store(storeOptions);
    //             wrapper = shallowMount(Projects, {store, localVue,});
    //         });
    //
    //         test('should render no projects', () => {
    //             expect(wrapper.findAll(Project)).toHaveLength(0);
    //         });
    //
    //         test('should not show "No projects available"', () => {
    //             expect(wrapper.text()).not.toContain('No projects available');
    //         });
    //
    //         test('should show "Loading projects..."', () => {
    //             expect(wrapper.text()).toContain('Loading projects...');
    //         });
    //     });
    //
    // });
    //
    // describe('if project are not loading', () => {
    //     function reset() {
    //         resetStore();
    //         storeOptions.state.projects.loadingProjects = false;
    //     }
    //
    //     beforeAll(reset);
    //
    //     describe('if projects are available', () => {
    //         let wrapper;
    //         const projects = [{}, {}, {}];
    //
    //         beforeAll(function () {
    //             reset();
    //             storeOptions.state.projects.projectsData = projects;
    //
    //             const store = new Vuex.Store(storeOptions);
    //             wrapper = shallowMount(Projects, {store, localVue,});
    //         });
    //
    //         test('should render all the projects', () => {
    //             expect(wrapper.findAll(Project)).toHaveLength(projects.length);
    //         });
    //
    //         test('should not show "No projects available"', () => {
    //             expect(wrapper.text()).not.toContain('No projects available');
    //         });
    //
    //         test('should not show "Loading projects..."', () => {
    //             expect(wrapper.text()).not.toContain('Loading projects...');
    //         });
    //     });
    //
    //     describe('if projects are not available', () => {
    //         let wrapper;
    //
    //         beforeAll(function () {
    //             reset();
    //             storeOptions.state.projects.projectsData = [];
    //
    //             const store = new Vuex.Store(storeOptions);
    //             wrapper = shallowMount(Projects, {store, localVue,});
    //         });
    //
    //         test('should render no projects', () => {
    //             expect(wrapper.findAll(Project)).toHaveLength(0);
    //         });
    //
    //         test('should show "No projects available"', () => {
    //             expect(wrapper.text()).toContain('No projects available');
    //         });
    //
    //         test('should not show "Loading projects..."', () => {
    //             expect(wrapper.text()).not.toContain('Loading projects...');
    //         });
    //     });
    // });

});


