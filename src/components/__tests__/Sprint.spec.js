import {iff, onNextTick} from "../../../tests/iff";
import Sprint from "../Sprint/Sprint";
import {shallowMount} from "@vue/test-utils";
import {console} from "vuedraggable/src/util/helper";
import Vuex from "vuex";

describe('Sprint.vue', () => {
    let props;
    let storeOptions;

    function reset() {
        props = {};
        storeOptions = {
            getters: {
                'taskEditPossible': () => true,
                'taskDeletePossible': () => true,
                'taskAddPossible': () => true,
                'sprintEditPossible': () => true,
                'sprintDeletePossible': () => true,
                'sprintsSortable': () => true
            }
        }
    }

    function sprintIsSpecified() {
        sprintIsSpecified.amountOfSpecifiedSprints = sprintIsSpecified.amountOfSpecifiedSprints || 0;
        // Need to get the argument like that because otherwise the beforeEach(...) call would pass a callback function
        // https://github.com/facebook/jest/issues/8313#issuecomment-482294236
        const sprint = arguments[0];
        if (typeof sprint !== 'undefined' && sprint !== null) {
            const obj = {
                ['sprintIsSpecifiedWithData_(' + (++sprintIsSpecified.amountOfSpecifiedSprints) + ')']: function () {
                    props.sprint = sprint;
                }
            };
            return obj['sprintIsSpecifiedWithData_(' + sprintIsSpecified.amountOfSpecifiedSprints + ')'];
        }
        props.sprint = {
            id: 1,
            title: 'Mock sprint title',
            tasks: [{}, {}, {}]
        };
    }

    function sprintIsNotSpecified() {
        delete props.sprint;
    }

    function shouldNotShowErrorsInConsole() {
        const consoleSpy = jest.spyOn(console, 'error');
        consoleSpy.mockImplementation(() => true);

        consoleSpy.mockClear();
        expect(shallowMountMocked).not.toThrowError();
        expect(consoleSpy).not.toBeCalled();
        consoleSpy.mockReset();
    }

    function shouldShowErrorsInConsole() {
        const consoleSpy = jest.spyOn(console, 'error');
        consoleSpy.mockImplementation(() => true);

        consoleSpy.mockClear();
        expect(shallowMountMocked).not.toThrowError();
        expect(consoleSpy).toBeCalledWith(expect.stringContaining('[Vue warn]: Missing required prop: "sprint"'));
        consoleSpy.mockReset();
    }

    function shouldNotRenderSprint(sprintComponent) {
        expect(sprintComponent.isEmpty()).toBeTruthy();
    }

    function shouldRenderSprint(sprintComponent) {
        expect(sprintComponent.isEmpty()).toBeFalsy();
    }

    async function shouldOpenSprintExpandableIfOpenedSprintIsCurrentSprint(sprintComponent) {
        expect.assertions(1);
        sprintComponent.setProps({openedSprint: 1});
        await onNextTick(sprintComponent, () => expect(sprintComponent.emitted('hasOpenedSprint')).toHaveLength(1));
    }

    async function shouldNotOpenSprintExpandableIfOpenedSprintIsNotCurrentSprint(sprintComponent) {
        expect.assertions(1);
        sprintComponent.setProps({openedSprint: 2});
        await onNextTick(sprintComponent, () => expect(sprintComponent.emitted('hasOpenedSprint')).not.toBeDefined());
    }

    function todayIs20190806(){
        const mockedToday = new Date('2019-08-06');
        Date.now = () => mockedToday;
    }

    function shallowMountMocked() {
        const store = new Vuex.Store(storeOptions);
        return shallowMount(Sprint, {propsData: props, store});
    }

    function shouldEverythingBeRendered(sprintComponent) {
        expect(sprintComponent.html()).toMatchSnapshot();
    }

    iff.reset = reset;
    iff(sprintIsSpecified).then(shouldNotShowErrorsInConsole);
    iff(sprintIsNotSpecified).then(shouldShowErrorsInConsole);

    iff.injectParameter = () => shallowMountMocked();

    iff(sprintIsNotSpecified).then(shouldNotRenderSprint);
    iff(sprintIsSpecified).then(shouldRenderSprint);
    iff(sprintIsSpecified).then(shouldOpenSprintExpandableIfOpenedSprintIsCurrentSprint);
    iff(sprintIsSpecified).then(shouldNotOpenSprintExpandableIfOpenedSprintIsNotCurrentSprint);

    iff(sprintIsSpecified({
        title: 'My Sprint Name',
        startDate: '2019-08-01',
        endDate: '2019-08-14',
        tasks: [ {}, {}, {} ]
    })).and(todayIs20190806).then(shouldEverythingBeRendered);

    iff.startTests();
});