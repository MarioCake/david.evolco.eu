import {iff, onNextTick} from "../../../tests/iff";
import {mount} from "@vue/test-utils";
import Task from "../Task/Task";
import Vuex from "vuex";

describe('Task.vue', () => {
    let props = {};
    let slots = {};

    function noTaskPropSpecified() {
        delete props.task;
    }

    function taskPropSpecified() {
        // Need to get the argument like that because otherwise the beforeEach(...) call would pass a callback function
        // https://github.com/facebook/jest/issues/8313#issuecomment-482294236
        const task = arguments[0];
        if (typeof task !== 'undefined' && task !== null) {
            return function taskPropSpecified() {
                props.task = task;
            };
        }
        props.task = {
            taskName: 'Mock Task Name',
            completed: false
        };
    }

    function taskIsCompleted() {
        props.task = {
            taskName: 'Completed Task',
            completed: true,
        }
    }

    function taskIsNotCompleted() {
        props.task = {
            taskName: 'Not Completed Task',
            completed: false,
        }
    }

    function shouldRenderEverything(taskComponent){
        expect(taskComponent.html()).toMatchSnapshot();
    }

    async function shouldStatusColorBeGreen(taskComponent) {
        await taskComponent.vm.$nextTick();
        expect(taskComponent.find('div.state.right').element.style.backgroundColor.replace(/ /g, '')).toEqual('rgb(55,156,34)')
    }

    async function shouldStatusColorBeRed(taskComponent) {
        expect.assertions(1);
        await onNextTick(taskComponent, () => expect(taskComponent.find('div.state.right').element.style.backgroundColor.replace(/ /g, '')).toEqual('rgb(200,57,44)'));
    }

    function shouldShowRequiredPropErrorInConsole() {
        let spy = jest.spyOn(console, 'error');
        spy.mockImplementation(() => true);
        const mountTask = () => mount(Task, {propsData: props});
        expect(mountTask).not.toThrowError();
        expect(spy).toBeCalledWith(expect.stringContaining("[Vue warn]: Missing required prop: \"task\""));
        spy.mockReset();
    }

    function shouldShowNoErrors(spy) {
        expect(spy).not.toBeCalled();

        spy.mockReset();
    }

    function shouldPropTaskBeRequired(taskComponent) {
        const taskProp = taskComponent.vm.$options.props.task;
        expect(taskProp.required).toBeTruthy();
    }

    function shouldSendEventOnDeleteClick(taskComponent) {
        taskComponent.find({name: 'v-icon'}).trigger('click');
        expect(taskComponent.emitted('deleteTask')).toHaveLength(1);
    }

    function shouldSendEventOnChangeStateClick(taskComponent) {
        taskComponent.find('div.state.right').trigger('click');
        expect(taskComponent.emitted('changeTaskState')).toHaveLength(1);
    }

    function shouldRenderTaskName(taskComponent) {
        expect(taskComponent.text()).toContain(props.task.taskName);
    }

    function shouldRenderSlot(taskComponent) {
        expect(taskComponent.text()).toContain('my prefix slot');
    }

    function prefixSlotSpecified() {
        slots.prefix = 'my prefix slot';
    }

    function prefixSlotNotSpecified() {
        delete slots.prefix;
    }

    function mountMocked() {
        const store = new Vuex.Store({
            getters: {
                'taskEditPossible': () => true,
                'taskDeletePossible': () => true
            }
        });
        return mount(Task, {propsData: props, store, slots});
    }

    iff.reset = function () {
        props = {};
    };
    const propsTest = {name: 'props'};
    const actions = {name: 'actions'};

    iff(noTaskPropSpecified).then(shouldShowRequiredPropErrorInConsole);

    iff.injectParameter = function () {
        return mountMocked();
    };
    iff(propsTest).then(shouldPropTaskBeRequired);
    iff(taskIsCompleted).then(shouldStatusColorBeGreen);
    iff(taskIsNotCompleted).then(shouldStatusColorBeRed);
    iff(taskPropSpecified).then(shouldRenderTaskName);
    iff(taskPropSpecified).and(prefixSlotSpecified)
        .then(shouldRenderSlot)
        .and(shouldRenderTaskName);
    iff(actions).then(shouldSendEventOnDeleteClick);
    iff(actions).then(shouldSendEventOnChangeStateClick);
    iff(taskPropSpecified).then(shouldRenderEverything);

    iff.injectParameter = function () {
        const consoleSpy = jest.spyOn(console, 'error');
        consoleSpy.mockImplementation(() => true);
        consoleSpy.mockClear();
        mountMocked();
        return consoleSpy;
    };
    iff(taskPropSpecified).and(prefixSlotNotSpecified).then(shouldShowNoErrors);

    iff.startTests();
});