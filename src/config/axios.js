import axios from 'axios';
import {store} from "../store/store";

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;
axios.interceptors.request.use(config => new Promise((resolve) => {
    function setHeaders(){
        config.headers.common['Authorization'] = `Bearer ${store.getters.accessToken}`;

        resolve(config);
    }

    if (!store.getters.isAuthenticated) {

        store.dispatch("authenticate").then(setHeaders);
    }else{
        setHeaders();
    }

}));

export default axios;
