import Vue from 'vue'
import './plugins/vuetify.js'
import axios from './config/axios.js'
import {store} from './store/store.js'
import App from './App'
import 'vuetify/dist/vuetify.css'
import {EventCallerMixin} from "./mixins/EventCallerMixin"
import {ValidationMixins} from "./mixins/Validation/ValidationMixins";

Vue.config.productionTip = false;

Vue.mixin(EventCallerMixin);
Vue.mixin(ValidationMixins);
Vue.prototype.$http = axios;

// noinspection JSIgnoredPromiseFromCall
store.dispatch('fetchProjects');

new Vue({
    store,
    render: h => h(App)
}).$mount('#app');


// Collapse trace in chrome workaround
if (window.console && window.console.trace) {
    const oldTrace = window.console.trace;
    window.console.trace = function () {
        if(typeof arguments[0] === 'string'){
            arguments[0] = '%c' + arguments[0];
            Array.prototype.splice.apply(arguments, [1, 0, 'font-weight: normal;']);
        }else{
            let format = '%c';
            if(typeof arguments[0] === 'undefined'){
                format = '%ctrace';
            }

            Array.prototype.splice.apply(arguments, [0, 0, format, 'font-weight: normal;']);
        }
        const newArgs = Object.entries(arguments).reduce(
            (arr, entry) => {
                arr[entry[0]] = entry[1];
                return arr
            },
            []
        );
        window.console.groupCollapsed(...newArgs);
        oldTrace.apply(this);
        window.console.groupEnd();
    }
}