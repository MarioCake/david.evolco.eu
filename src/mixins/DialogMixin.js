export const Dialog = {
    props: {
        visible: {
            type: Boolean,
            required: true
        }
    },
    data(){
        return {
            setTriggered: false,
        };
    },
    watch: {
        visible(isVisible, oldValue) {
            if(this.setTriggered){
                this.setTriggered = false;
                return;
            }

            if (isVisible !== oldValue) {
                this.sendOpenOrCloseEvent(isVisible);
            }
        }
    },
    computed: {
        $_isVisible: {
            get() {
                return this.visible;
            },
            set(isVisible) {
                if (this.visible !== isVisible) {
                    this.setTriggered = true;
                    this.sendOpenOrCloseEvent(isVisible);
                }
            }
        }
    },
    methods: {
        sendOpenOrCloseEvent(isVisible) {
            if (isVisible && this.onOpenDialog) {
                this.onOpenDialog();
            } else if (!isVisible && this.onCloseDialog) {
                this.onCloseDialog(!this.setTriggered);
            }
        }
    }
};