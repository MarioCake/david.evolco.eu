export const EventCallerMixin = {
    data() {
        return {}
    },
    created() {
        const viewModel = this;

        // noinspection JSUnusedGlobalSymbols
        this.$eventCaller = new Proxy({}, {
            get: function (proxy, propertyName) {
                return viewModel.$emit.bind(viewModel, propertyName);
            }
        });
    }
};