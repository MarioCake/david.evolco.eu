import {ValidationRequiredMixin} from "./ValidationRequiredMixin";

export const ValidationMixins = {
    mixins: [
        ValidationRequiredMixin
    ]
};