export const ValidationRequiredMixin = {
    data() {
        return {
            required: [
                v => !!v || "Required"
            ]
        }
    }
};