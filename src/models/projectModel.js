export default class ProjectModel {
    constructor(id, projectName) {
        this.id = id;
        this.projectName = projectName;
        this.sprints = [];
    }

}