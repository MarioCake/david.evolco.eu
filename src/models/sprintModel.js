export default class SprintModel {
    constructor(id, title, startDate, endDate) {
        this.title = title;
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tasks = [];
    }
}