export default class TaskModel {
    constructor(id, taskName, completed) {
        this.id = id;
        this.taskName = taskName;
        this.completed = completed || false;
    }
}