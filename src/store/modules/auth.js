import axios from "axios";

export const auth = {
    state: {
        token: null,
        tokenExpiresAt: null,
        currentUser: null
    },
    mutations: {
        storeAuthData(state, {tokenExpiresAt, token}) {
            state.token = token;
            state.tokenExpiresAt = tokenExpiresAt;
        },
        storeCurrentUser(state, currentUser) {
            state.currentUser = currentUser;
        },
    },
    actions: {
        handleAuthData({commit, dispatch}, payload) {
            const now = new Date();
            const tokenExpiresAt = new Date(now.getTime() + payload.expires_in * 1000);
            const refreshAt = new Date(now.getTime() + payload.expires_in * 666); // refresh access token when there's less than 1/3rd left on its validity time
            commit('storeAuthData', {
                tokenExpiresAt,
                token: payload.access_token
            });

            localStorage.evoAccessToken = payload.access_token;
            localStorage.evoRefreshToken = payload.refresh_token;
            localStorage.evoTokenRefreshAt = refreshAt.toString();
            localStorage.evoTokenExpiresAt = tokenExpiresAt.toString();

            return dispatch('getCurrentUser');
        },
        login({dispatch}, authData) {
            const tokenInstance = axios.create({
                baseURL: process.env.VUE_APP_OAUTH_BASE_URL
            });
            return tokenInstance.post('token', {
                scope: process.env.VUE_APP_SCOPE,
                grant_type: process.env.VUE_APP_GRANT_TYPE,
                client_id: process.env.VUE_APP_CLIENT_ID,
                client_secret: process.env.VUE_APP_CLIENT_SECRET,
                username: authData.username,
                password: authData.password
            }).then(response => {
                return dispatch('handleAuthData', response.data);
            });
        },
        refreshToken({dispatch}, refreshToken) {
            const tokenInstance = axios.create({
                baseURL: process.env.VUE_APP_OAUTH_BASE_URL
            });
            return tokenInstance.post('token', {
                scope: process.env.VUE_APP_SCOPE,
                grant_type: 'refresh_token',
                client_id: process.env.VUE_APP_CLIENT_ID,
                client_secret: process.env.VUE_APP_CLIENT_SECRET,
                refresh_token: refreshToken
            }).then(response => {
                return dispatch('handleAuthData', response.data);
            });
        },
        tryAutoLogin({commit, dispatch}) {
            const token = localStorage.evoAccessToken;
            if (!token) {
                return;
            }
            let refreshAt = localStorage.evoTokenRefreshAt;
            if (refreshAt) {
                refreshAt = new Date(refreshAt);
            }
            const now = new Date();
            if (!refreshAt || now.getTime() >= refreshAt.getTime()) {
                const refreshToken = localStorage.evoRefreshToken;
                if (refreshToken) {
                    dispatch('refreshToken', refreshToken);
                }
                return;
            }
            let tokenExpiresAt = localStorage.evoTokenExpiresAt;
            if (tokenExpiresAt) {
                tokenExpiresAt = new Date(tokenExpiresAt);
            }
            commit('storeAuthData', {
                tokenExpiresAt,
                token
            });
            return Promise.resolve();
        },
        authenticate({dispatch, getters}) {
            return dispatch("tryAutoLogin")
                .then(() => {
                    if (getters.isAuthenticated) {
                        return Promise.resolve();
                    }

                    // TODO: Extract into userlogin!
                    return dispatch("login", {
                        username: process.env.VUE_APP_USERNAME,
                        password: process.env.VUE_APP_PASSWORD
                    });
                })
        },
        getCurrentUser({commit, getters}) {
            if (!getters.isAuthenticated) {
                return;
            }
            return axios.get('user')
                .then(response => {
                    commit('storeCurrentUser', response.data);
                    localStorage.evoCurrentUser = JSON.stringify(response.data);
                });
        },
    },
    getters: {
        accessToken(state) {
            return state.token;
        },
        isAuthenticated(state) {
            const now = new Date();
            return state.token &&
                state.tokenExpiresAt &&
                now.getTime() < state.tokenExpiresAt.getTime();
        }
    }
};