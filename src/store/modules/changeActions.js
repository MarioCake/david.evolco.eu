export const changeActions = {
    state: {
        anyChangePossible: true,
        anySprintChangePossible: true,
        anyProjectChangePossible: true,
        anyTaskChangePossible: true,

        taskAddPossible: true,
        taskDeletePossible: true,
        taskEditPossible: true,
        sprintAddPossible: true,
        sprintDeletePossible: true,
        sprintEditPossible: true,
        projectAddPossible: true,
        projectDeletePossible: true,
        projectEditPossible: true,

        sprintsSortable: true,
    },
    getters: {
        anyChangePossible(state) {
            return state.anyChangePossible;
        },
        anyTaskChangePossible(state, getters) {
            return getters.anyChangePossible && state.anyTaskChangePossible
                && (state.taskEditPossible || state.taskAddPossible || state.taskDeletePossible);
        },
        anySprintChangePossible(state, getters) {
            return getters.anyChangePossible && state.anySprintChangePossible
                && (state.sprintEditPossible || state.sprintAddPossible || state.sprintDeletePossible);
        },
        anyProjectChangePossible(state, getters) {
            return getters.anyChangePossible && state.anyProjectChangePossible
                && (state.projectEditPossible || state.projectAddPossible || state.projectDeletePossible);
        },
        taskAddPossible(state, getters) {
            return getters.anyTaskChangePossible && state.taskAddPossible;
        },
        taskDeletePossible(state, getters) {
            return getters.anyTaskChangePossible && state.taskDeletePossible;
        },
        taskEditPossible(state, getters) {
            return getters.anyTaskChangePossible && state.taskEditPossible;
        },
        sprintAddPossible(state, getters) {
            return getters.anySprintChangePossible && state.sprintAddPossible;
        },
        sprintDeletePossible(state, getters) {
            return getters.anySprintChangePossible && state.sprintDeletePossible;
        },
        sprintEditPossible(state, getters) {
            return getters.anySprintChangePossible && state.sprintEditPossible;
        },
        projectAddPossible(state, getters) {
            return getters.anyProjectChangePossible && state.projectAddPossible;
        },
        projectDeletePossible(state, getters) {
            return getters.anyProjectChangePossible && state.projectDeletePossible;
        },
        projectEditPossible(state, getters) {
            return getters.anyProjectChangePossible && state.projectEditPossible;
        },

        sprintsSortable(state){
            return state.sprintsSortable;
        }
    }
};