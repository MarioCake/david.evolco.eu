import {MiscUtils} from "../../utils/MiscUtils";
import {ProjectUtils} from "../../utils/ProjectUtils";
import moment from "moment";
import axios from "axios";
import Vue from "vue";

const PROJECTS_PATH = 'projects/';

const projectsFromLocalStorage = JSON.parse('{}');

export default {
    state: {
        projectName: '',
        selectedProject: null,
        deleteProjectDialogVisible: false,
        editProjectDialogVisible: false,
        projectsData: projectsFromLocalStorage,
        loadingProjects: projectsFromLocalStorage.length < 0
    },
    mutations: {
        openEditProjectDialog(state, project) {
            state.selectedProject = project;
            state.editProjectDialogVisible = true;
        },
        openDeleteProjectDialog(state, project) {
            state.selectedProject = project;

            state.deleteProjectDialogVisible = true;
        },
        addProject(state, project) {
            Vue.set(state.projectsData, project.id, project);
        },
        saveProject(state, newProject) {
            if (newProject) {
                state.projectsData[newProject.id] = newProject;
            }
            state.editProjectDialogVisible = false;
        },
        deleteProject(state, shouldDeleteProject) {
            if (shouldDeleteProject) {
                Vue.delete(state.projectsData, state.selectedProject.id);
            }

            state.deleteProjectDialogVisible = false;
        },
        changeSprintOrder(state, {projectId, fromIndex, toIndex}) {
            const project = state.projectsData[projectId];
            project.sprints.splice(toIndex, 0, project.sprints.splice(fromIndex, 1)[0]);
        },
        initProjects(state, projects) {
            state.projectsData = projects.reduce((result, project) => {
                result[project.id] = project;
                return result;
            }, {});
        },
        changeSprintProject(state, {toIndex, fromProject, sprint}){
            const projectFrom = state.projectsData[fromProject];
            const projectTo = state.projectsData[sprint.project_id];

            const sprintIndex = projectFrom.sprints.map(sprint => sprint.id).indexOf(sprint.id);

            projectFrom.sprints.splice(sprintIndex, 1);
            projectTo.sprints.splice(toIndex, 0, sprint);
        },
        startLoadingProjects(state){
            state.loadingProjects = true;
        },
        endLoadingProjects(state){
            state.loadingProjects = false;
        }
    },
    actions: {
        openEditProjectDialog({commit}, projectId) {
            commit('openEditProjectDialog', projectId);
        },
        openDeleteProjectDialog({commit}, project) {
            commit("openDeleteProjectDialog", project);
        },
        addProject({commit}, project) {
            project = ProjectUtils.transformProjectFromFrontend(project);
            if (!project) {
                commit('addProject', false);
                return;
            }

            return axios.post(PROJECTS_PATH, project)
                .then((response) => {
                    commit('addProject', response.data);
                })
        },
        saveProject({commit, state}, project) {
            if(!project){
                commit("saveProject", false);
                return;
            }

            project = {...state.projectsData[project.id], ...ProjectUtils.transformProjectFromFrontend(project)};

            return axios.put(PROJECTS_PATH + project.id, {...project, sprints: []})
                .then((response) => {
                    commit("saveProject", response.data);
                });
        },
        deleteProject({commit, state}, shouldDeleteProject) {
            if (!shouldDeleteProject) {
                commit("deleteProject", false);
                return;
            }

            return axios.delete(PROJECTS_PATH + state.selectedProject.id)
                .then(() => {
                    commit("deleteProject", true);
                });
        },
        changeSprintOrder({commit}, {projectId, fromIndex, toIndex}) {
            commit('changeSprintOrder', {projectId, fromIndex, toIndex});
        },
        fetchProjects({commit, dispatch}) {
            commit('startLoadingProjects');
            return axios.get(PROJECTS_PATH)
                .then(response => {
                    let projects = response.data.data;

                    return dispatch('fetchSprintsForProjects', projects)
                })
                .then((projectsWithSprints) => {
                    commit('initProjects', projectsWithSprints);
                })
                .finally(() => {
                    commit('endLoadingProjects');
                })
        },
        saveSprintWithProjectId({commit, state}, {sprint, projectId}){
            commit('saveSprint', {project: state.projectsData[projectId], sprint});
        },

        fetchSprintsForProject(_, project) {
            return axios.get(PROJECTS_PATH + project.id)
                .then((projectResponse) => {
                    return projectResponse.data;
                });
        },
        fetchSprintsForProjects({dispatch}, projects) {
            return Promise.all(
                projects.map(project => dispatch('fetchSprintsForProject', project))
            );
        },
    },

    getters: {
        projects(state){
            return Object.values(state.projectsData).map(ProjectUtils.transformProjectFromBackend);
        },
        loadingProjects(state){
            return state.loadingProjects;
        },
        lastEndDate(state, getters) {
            const additionalTime = ProjectUtils.calculateAdditionalTimeToCompleteProjects(getters.projects);
            let endDate = new Date(getters.projects.reduce(ProjectUtils.lastProjectEndDate, new Date()).getTime() + additionalTime.getTime());

            return MiscUtils.startOfDay(endDate);
        },
        lastEndDateFormatted(state, getters) {
            return moment(getters.lastEndDate).format("YYYY-MM-DD")
        }
    }
}