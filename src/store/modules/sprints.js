import SprintModel from "../../models/sprintModel";
import axios from "axios";
import {SprintUtils} from "../../utils/SprintUtils";
import {ProjectUtils} from "../../utils/ProjectUtils";
import moment from "moment";

const SPRINTS_PATH = 'project-sprints/';

export const sprints = {
    state: {
        selectedSprint: null,
        selectedProject: null,
        deleteSprintDialogVisible: false,
        addSprintDialogVisible: false,
        editSprintDialogVisible: false,

        sprintMoveToProject: null,
    },

    mutations: {
        deleteSprint(state, model) {
            state.deleteSprintDialogVisible = false;
            if (!model) {
                return;
            }

            const project = model;
            const sprintIndex = project.sprints.map(sprint => sprint.id).indexOf(state.selectedSprint.id);
            project.sprints.splice(sprintIndex, 1);
        },
        addSprint(state, model) {
            state.addSprintDialogVisible = false;

            if (!model) {
                return;
            }

            const {sprint, project} = model;
            project.sprints.push(sprint);
        },
        openDeleteSprintDialog(state, {project, sprint}) {
            state.selectedProject = project;
            state.selectedSprint = sprint;

            state.deleteSprintDialogVisible = true;
        },
        openAddSprintDialog(state, project) {
            const sprintStartDate = ProjectUtils.lastProjectEndDate(new Date(), project);
            const sprintEndDate = new Date(sprintStartDate);
            sprintEndDate.setDate(sprintEndDate.getDate() + 7);

            state.selectedSprint = new SprintModel(null, null, moment(sprintStartDate).format('YYYY-MM-DD'), moment(sprintEndDate).format('YYYY-MM-DD'));
            state.selectedProject = project;
            state.addSprintDialogVisible = true;
        },
        openEditSprintDialog(state, {sprint, project}) {
            state.selectedSprint = sprint;
            state.selectedProject = project;
            state.editSprintDialogVisible = true;
        },
        saveSprint(state, model) {
            state.editSprintDialogVisible = false;

            if (!model) {
                return;
            }

            const {sprint: newSprint, project} = model;

            Object.assign(project.sprints.find(sprint => sprint.id === newSprint.id), newSprint);
        },
        prepareChangeSprintProject(state, {toProject, newIndex}) {
            state.sprintMoveToProject = toProject;
            state.sprintMoveToProjectIndex = newIndex;
        }
    },

    actions: {
        deleteSprint({commit, state, rootState}, shouldDeleteSprint) {
            if (!shouldDeleteSprint) {
                commit("deleteSprint", false);
                return;
            }

            return axios.delete(SPRINTS_PATH + state.selectedSprint.id)
                .then(() => {
                    const project = rootState.projects.projectsData[state.selectedProject.id];
                    commit("deleteSprint", project);
                });
        },
        addSprint({commit, state, rootState}, sprint) {
            if (!sprint) {
                commit("addSprint", false);
                return;
            }

            const project = rootState.projects.projectsData[state.selectedProject.id];
            sprint = SprintUtils.transformSprintFromFrontend(sprint);
            sprint.project_id = project.id;

            return axios.post(SPRINTS_PATH, sprint)
                .then((response) => {
                    commit("addSprint", {sprint: response.data, project});
                });
        },
        openDeleteSprintDialog({commit}, {project, sprint}) {
            commit("openDeleteSprintDialog", {project, sprint})
        },
        openAddSprintDialog({commit}, project) {
            commit('openAddSprintDialog', project);
        },
        openEditSprintDialog({commit}, {project, sprint}) {
            commit('openEditSprintDialog', {project, sprint});
        },
        saveSprint({commit, rootState, state}, sprint) {
            if (!sprint) {
                commit('saveSprint', false);
                return;
            }
            const project = rootState.projects.projectsData[state.selectedProject.id];
            const origSprint = project.sprints.find(searchSprint => searchSprint.id === sprint.id);
            sprint = SprintUtils.transformSprintFromFrontend(sprint);


            return axios.put(SPRINTS_PATH + sprint.id, {...origSprint, ...sprint})
                .then((response) => {
                    commit('saveSprint', {sprint: response.data, project});
                });
        },
        changeSprintProject({commit, state, dispatch}, {sprint, oldIndex}) {
            commit('changeSprintProject', {
                fromProject: sprint._orig.project_id,
                sprint: {...sprint, _orig: {...sprint._orig, project_id: state.sprintMoveToProject}},
                toIndex: state.sprintMoveToProjectIndex
            });

            return axios.put(SPRINTS_PATH + sprint.id, {
                ...SprintUtils.transformSprintFromFrontend(sprint),
                project_id: state.sprintMoveToProject
            })
                .then((response) => {
                    return dispatch('saveSprintWithProjectId', {
                        sprint: SprintUtils.transformSprintFromBackend(response.data),
                        projectId: state.sprintMoveToProject
                    });
                })
                .catch(() => {
                    commit('changeSprintProject', {
                        fromProject: state.sprintMoveToProject,
                        sprint,
                        toIndex: oldIndex
                    });
                });
        },
        prepareChangeSprintProject({commit}, {toProject, newIndex}) {
            commit('prepareChangeSprintProject', {toProject, newIndex});
        }
    },

    getters: {}
};
