import TaskModel from "../../models/taskModel";
import axios from "axios";

const USER_STORIES_PATH = 'project-user-stories/';

export default {
    state: {
        addTaskDialogVisible: false,
        deleteTaskDialogVisible: false,
        selectedTask: null,
        selectedSprint: null,
        selectedProject: null
    },
    mutations: {
        addTask(state, model) {
            state.addTaskDialogVisible = false;
            if (!model) {
                return;
            }

            const {sprint, task} = model;
            sprint.user_stories.push(task);
        },
        openDeleteTaskDialog(state, {sprint, task, project}) {
            state.selectedTask = task;
            state.selectedSprint = sprint;
            state.selectedProject = project;

            state.deleteTaskDialogVisible = true;
        },
        openAddTaskDialog(state, {project, sprint}) {
            state.selectedTask = new TaskModel(null, null);
            state.selectedSprint = sprint;
            state.selectedProject = project;

            state.addTaskDialogVisible = true;
        },
        deleteTask(state, model) {
            state.deleteTaskDialogVisible = false;
            if (!model) {
                return;
            }

            const sprint = model;
            const taskIndex = sprint.user_stories.map(userStory => userStory.id).indexOf(state.selectedTask.id);
            sprint.user_stories.splice(taskIndex, 1);
        },
        changeTaskState(storeState, {project, sprint, task, state}) {
            project.sprints.find(spr => spr.id === sprint.id).tasks.find(tsk => tsk.id === task.id).completed = state;
        }
    },
    actions: {
        addTask({commit, state, rootState}, task) {
            if (!task) {
                commit('addTask', false);
                return;
            }

            return axios.post(USER_STORIES_PATH, {
                project_id: state.selectedProject.id,
                sprint_id: state.selectedSprint.id,
                status: 'new',
                title: task.taskName
            })
                .then(response => {
                    const project = rootState.projects.projectsData[state.selectedProject.id];
                    const sprint = project.sprints.find(searchSprint => searchSprint.id === state.selectedSprint.id);
                    commit('addTask', {task: response.data, sprint});
                })
                .catch(() => {
                    commit('addTask', false)
                })
        },
        openDeleteTaskDialog({commit}, {sprint, task, project}) {
            commit('openDeleteTaskDialog', {sprint, task, project});
        },
        openAddTaskDialog({commit}, {project, sprint}) {
            commit('openAddTaskDialog', {project, sprint});
        },
        deleteTask({commit, state, rootState}, shouldDeleteTask) {
            if (!shouldDeleteTask) {
                commit("deleteTask", false);
                return;
            }


            return axios.delete(USER_STORIES_PATH + state.selectedTask.id)
                .then(() => {
                    const project = rootState.projects.projectsData[state.selectedProject.id];
                    const sprint = project.sprints.find(searchSprint => searchSprint.id === state.selectedSprint.id);
                    commit("deleteTask", sprint);
                })
                .catch(() => {
                    commit("deleteTask", false);
                })
        },
        changeTaskState({commit}, {project, sprint, task, state}) {
            commit("changeTaskState", {project, sprint, task, state});
        }
    },
    getters: {},
};