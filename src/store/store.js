import Vue from 'vue'
import Vuex from 'vuex'
import tasks from './modules/tasks';
import projects from './modules/projects'
import {sprints} from "./modules/sprints";
import {auth} from "./modules/auth";
import {changeActions} from './modules/changeActions';

Vue.use(Vuex);
const store = new Vuex.Store({
    modules: {
        tasks,
        projects,
        sprints,
        auth,
        changeActions
    },
    state: {},
    mutations: {},
    actions: {},
    getters: {}
});

export {store};
