export const DurationViewer = {
    addPlural(text, amount) {
        return text + (amount === 1 ? '' : 's');
    },
    addDurationText(currentDuration, timeUnit, time) {
        if (time) {
            if (currentDuration.length !== 0) {
                currentDuration += ", ";
            }

            currentDuration += this.addPlural(time + ' ' + timeUnit, time);
        }

        return currentDuration;
    },
    addDuration(currentDuration, timeUnit, endDate, startDate) {
        const difference = endDate.diff(startDate, timeUnit);
        endDate.subtract(difference, timeUnit);
        currentDuration = this.addDurationText(currentDuration, timeUnit, difference);

        return currentDuration;
    },
};