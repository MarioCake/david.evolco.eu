export const MathUtils = {
    logOf2: Math.log(2),
    timeZoomForGraph(timeInDays, chartWidth, timeScale = 1000){
        let logParameter = chartWidth * timeScale;
        logParameter -= 147744000 * timeInDays;
        logParameter *= -3;
        logParameter /= 5 * chartWidth;

        return Math.log(logParameter) / this.logOf2;
    }
};