import moment from "moment";

const today = new Date();
today.setHours(0, 0, 0);

export const MiscUtils = {
    ONE_DAY_IN_MILLISECONDS: 1000 * 60 * 60 * 24,
    today,
    startOfDay(date) {
        date = moment(date);
        const result = date.startOf('day');
        return result.toDate();
    },
    toPercentage(val){
        return Math.round(val * 10000) / 100;
    },
    parseDate(date, format) {
        if(date instanceof Date){
            return new Date(date);
        }

        format = format || 'YYYY-MM-DD';
        return moment(date, format).toDate();
    },
    format(date, format) {
        format = format || "YYYY-MM-DD";
        return moment(date).format(format);
    }
};