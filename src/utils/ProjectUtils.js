import {SprintUtils} from "./SprintUtils";
import {MiscUtils} from "./MiscUtils";

export const ProjectUtils = {
    calculateDurationFromAllProjects(projects) {
        const start = this.startDateFromAllProjects(projects);
        const end = this.endDateFromAllProjects(projects);

        return end.getTime() - start.getTime();
    },
    endDateFromAllProjects(projects, hideCompleted) {
        return new Date(Math.max(...projects.map(project => this.calculateEndDate(project, hideCompleted))));
    },
    startDateFromAllProjects(projects, hideCompleted) {
        return new Date(Math.min(...projects.map(project => this.calculateStartDate(project, hideCompleted))))
    },
    calculateStartDate(project, hideCompleted) {
        return SprintUtils.calculateStartDate(project.sprints, hideCompleted);
    },
    calculateEndDate(project, hideCompleted) {
        return SprintUtils.calculateEndDate(project.sprints, hideCompleted);
    },
    calculateProjectDuration(project, hideCompleted = false) {
        const startDate = this.calculateStartDate(project, hideCompleted);
        const endDate = this.calculateEndDate(project, hideCompleted);

        return endDate.getTime() - startDate.getTime();
    },
    calculateProgress(project) {
        if(project.sprints.length === 0){
            return 0;
        }

        let totalSprintDuration = 0;
        let totalCompletionRate = 0;
        for (const sprint of project.sprints) {
            const sprintDuration = SprintUtils.calculateSprintDuration(sprint);
            totalSprintDuration += sprintDuration;
            totalCompletionRate += SprintUtils.sprintCompletionRate(sprint) * sprintDuration;
        }

        totalCompletionRate /= project.sprints.length * totalSprintDuration;

        return totalCompletionRate;
    },
    projectCompleted(project){
        return this.calculateProgress(project) === 1;
    },
    projectEnded(project){
        return this.calculateEndDate(project) < MiscUtils.today;
    },
    projectStarted(project){
        return this.calculateStartDate(project) <= MiscUtils.today;
    },
    projectRunning(project){
        return this.projectStarted(project) && !this.projectEnded(project);
    },
    amountOfCompletedProjects(projects){
        return projects.reduce((completed, project) => completed + (this.projectCompleted(project) ? 1 : 0), 0);
    },
    calculateProgressOfAllProjects(projects){
        if(projects.length === 0){
            return 0;
        }

        let totalProjectsDuration = 0;
        let totalCompletionRate = 0;
        for (const project of projects) {
            const projectDuration = ProjectUtils.calculateProjectDuration(project);
            totalProjectsDuration += projectDuration;
            totalCompletionRate += ProjectUtils.calculateProgress(project) * projectDuration;
        }

        totalCompletionRate /= projects.length * totalProjectsDuration;

        return totalCompletionRate;
    },
    lastProjectEndDate(lastDate, project) {
        let projectEndDate = project.sprints.reduce(SprintUtils.lastSprintEndDate, new Date(0));

        if (!lastDate || lastDate < projectEndDate) {
            return projectEndDate
        }

        return lastDate;
    },
    calculateAdditionalTimeToCompleteProjects(projects) {
        return new Date(projects.reduce((additionalTime, project) => additionalTime +
            this.calculateAdditionalTimeToCompleteProject(project).getTime(),
            0));
    },
    calculateAdditionalTimeToCompleteProject(project) {
        const dueSprintsNotDone = project.sprints.filter(sprint => SprintUtils.sprintEnded(sprint) && !SprintUtils.sprintCompleted(sprint));

        return SprintUtils.calculateTimeToCompleteSprints(dueSprintsNotDone);
    },
    transformProjectFromBackend(project) {
        if(typeof project === 'undefined' || project === null){
            return null;
        }

        return {
            id: project.id,
            projectName: project.title,
            endDate: project.deadline,
            startDate: project.estimated_beginning_at,
            sprints: (project.sprints || []).map(SprintUtils.transformSprintFromBackend),
        };
    },
    transformProjectFromFrontend(project) {
        if(typeof project === 'undefined' || project === null){
            return null;
        }

        return {
            id: project.id,
            title: project.projectName,
            status: 'planned',
        };
    }
};