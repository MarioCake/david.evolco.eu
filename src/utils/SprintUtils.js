import moment from "moment";
import {TaskUtils} from "./TaskUtils";
import {MiscUtils} from "./MiscUtils";

export const SprintUtils = {

    colors: {
        sprintEnded: "#FF7777",
        sprintEndsOneWeek: "rgb(214, 214, 64)",
        sprintFine: "lightgreen"
    },
    getColor(sprint) {
        if (this.sprintEnded(sprint)) {
            return this.colors.sprintEnded;
        }
        if (this.sprintEndsOneWeek(sprint)) {
            return this.colors.sprintEndsOneWeek;
        }

        return this.colors.sprintFine;
    },
    calculateEndDate(sprints, hideCompleted = false) {
        if (sprints.length === 0) {
            return new Date();
        }

        return sprints.reduce((endDate, sprint) => {
            if (hideCompleted && this.sprintCompleted(sprint)) {
                return endDate;
            }

            if (!endDate) {
                return new Date(sprint.endDate);
            }
            return new Date(Math.max(endDate.getTime(), new Date(sprint.endDate).getTime()))
        }, null);
    },
    calculateStartDate(sprints, hideCompleted = false) {
        if (sprints.length === 0) {
            return new Date();
        }

        const result = sprints.reduce((startDate, sprint) => {
            if (hideCompleted && this.sprintCompleted(sprint)) {
                return startDate;
            }
            if (!startDate) {
                return new Date(sprint.startDate);
            }
            return new Date(Math.min(startDate.getTime(), new Date(sprint.startDate).getTime()))
        }, null);

        if(result === null){
            return new Date();
        }

        return result;
    },
    sprintEnded(sprint) {
        return MiscUtils.parseDate(sprint.endDate) < MiscUtils.today;
    },
    sprintCompleted(sprint) {
        return this.sprintCompletionRate(sprint) === 1;
    },
    amountOfCompletedSprints(sprints) {
        return sprints.reduce((completed, sprint) => completed + (this.sprintCompleted(sprint) ? 1 : 0), 0);
    },
    sprintCompletionRate(sprint) {
        const sprintTasks = (sprint.tasks || []);

        const allTasks = sprintTasks.length;

        if (allTasks === 0) {
            return 0;
        }
        const completedTasks = TaskUtils.amountOfCompletedTasks(sprintTasks);

        return completedTasks / allTasks;
    },
    sprintRunning(sprint) {
        return this.sprintStarted(sprint) && !this.sprintEnded(sprint);
    },
    sprintStarted(sprint) {
        return MiscUtils.parseDate(sprint.startDate) <= MiscUtils.today
    },
    sprintEndsOneWeek(sprint) {
        const inOneWeek = new Date();
        inOneWeek.setDate(inOneWeek.getDate() + 7);
        return MiscUtils.parseDate(sprint.endDate) <= inOneWeek;
    },

    lastSprintEndDate(lastDate, sprint) {
        if (SprintUtils.sprintCompleted(sprint)) {
            return lastDate;
        }

        const sprintEndDate = new Date(sprint.endDate);

        if (!lastDate || lastDate < sprintEndDate) {
            return sprintEndDate
        }

        return lastDate;
    },
    calculateSprintDuration(sprint) {
        const startDate = MiscUtils.parseDate(sprint.startDate);
        const endDate = MiscUtils.parseDate(sprint.endDate);

        return endDate.getTime() - startDate.getTime();
    },
    calculateTimeToCompleteSprint(sprint) {
        const startDate = moment(new Date(sprint.startDate));
        const endDate = moment(new Date(sprint.endDate));

        let difference = endDate.diff(startDate);
        difference -= difference * SprintUtils.sprintCompletionRate(sprint);

        return difference;
    },
    calculateTimeToCompleteSprints(sprints) {
        return new Date(sprints.reduce((time, sprint) => time + this.calculateTimeToCompleteSprint(sprint), 0));
    },
    transformSprintFromBackend(sprint) {
        return {
            id: sprint.id,
            title: sprint.sprint_name,
            startDate: sprint.starts_at,
            endDate: sprint.deadline,
            tasks: (sprint.user_stories || []).map(TaskUtils.fromUserStoryToTask),
        };
    },
    transformSprintFromFrontend(sprint) {
        return {
            id: sprint.id,
            sprint_name: sprint.title,
            starts_at: sprint.startDate,
            deadline: sprint.endDate,
            advanced_payment_status: 'irrelevant',
            contract_status: 'irrelevant',
            final_payment_status: 'irrelevant',
            status: 'started'
        }
    }
};