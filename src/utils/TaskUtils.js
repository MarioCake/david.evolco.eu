export const TaskUtils = {
    fromUserStoryToTask(userStory) {
        return {
            id: userStory.id,
            taskName: userStory.title,
            completed: userStory.status === 'done'
        };
    },
    amountOfCompletedTasks(tasks){
        return tasks.reduce((sum, task) => sum + (task.completed ? 1 : 0), 0);
    }
};