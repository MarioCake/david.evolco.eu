function getConditionText(condition) {
    if (typeof condition === 'function') {
        return condition.name.match(/((^|[A-Z])[a-z]+(\([0-9]+\))?)|([0-9]+|\([0-9]+\))/g).join(" ").toLowerCase();
    }

    return condition.name;
}

function getConditionMethod(condition) {
    if (typeof condition === 'function') {
        return condition;
    }

    return condition.method;
}

function getConditionObject(condition) {
    const conditionText = getConditionText(condition);
    const conditionFunction = getConditionMethod(condition);

    return {
        text: conditionText,
        method: conditionFunction,
    }
}

const STRIP_COMMENTS = /(\/\/.*$)|(\/\*[\s\S]*?\*\/)|(\s*=[^,\)]*(('(?:\\'|[^'\r\n])*')|("(?:\\"|[^"\r\n])*"))|(\s*=[^,\)]*))/mg;
const ARGUMENT_NAMES = /([^\s,]+)/g;

function getParamNames(func) {
    const fnStr = func.toString().replace(STRIP_COMMENTS, '');
    let result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
    if (result === null)
        result = [];
    return result;
}

function generateTestMethod(expectedResult) {
    const {text, method} = getConditionObject(expectedResult);

    const injectParameter = iff.injectParameter;
    return function () {
        if (injectParameter) {
            const methodParameters = getParamNames(method);
            const isAsync = method.constructor.name === 'AsyncFunction';
            let testMethod;
            if (methodParameters.pop() === 'done') {
                if (isAsync) {
                    testMethod = async function (done) {
                        await method.apply(this, [injectParameter(), done]);
                    };
                }else{
                    testMethod = function (done) {
                        return method.apply(this, [injectParameter(), done]);
                    };
                }
            } else {
                if (isAsync) {
                    testMethod = async function () {
                        await method.apply(this, [injectParameter()]);
                    };
                }else{
                    testMethod = function () {
                        return method.apply(this, [injectParameter()]);
                    };
                }

            }
            test(text, testMethod);
        } else {
            test(text, method);
        }
    };
}

export function onNextTick(component, testMethod) {
    return new Promise((resolve) => {
        component.vm.$nextTick(() => {
            resolve();
        });
    }).then(testMethod);
}

export function iff(ifCondition) {
    iff.tests = iff.tests || {describes: {}};
    const ifConditionObject = getConditionObject(ifCondition);

    if (typeof ifCondition === 'function') {
        ifConditionObject.text = 'if ' + ifConditionObject.text;
    }
    const conditions = [ifConditionObject];
    const result = {
        and(andCondition) {
            const conditionObject = getConditionObject(andCondition);
            if (typeof andCondition === 'function') {
                conditionObject.text = 'if ' + conditionObject.text;
            }
            conditions.push(conditionObject);

            return result;
        },
        then(expectedResult) {
            const testMethod = generateTestMethod(expectedResult);

            let currentDescribes = iff.tests;

            for (const conditionObject of conditions) {
                currentDescribes.describes[conditionObject.text] = currentDescribes.describes[conditionObject.text] || {describes: {}};
                currentDescribes = currentDescribes.describes[conditionObject.text];
                currentDescribes.conditionSetup = conditionObject.method;
            }

            currentDescribes.tests = currentDescribes.tests || [];
            currentDescribes.tests.push(testMethod);

            const expectedResultObj = {
                and(expectedResult) {
                    const testMethod = generateTestMethod(expectedResult);
                    currentDescribes.tests.push(testMethod);

                    return expectedResultObj;
                }
            };

            return expectedResultObj;
        }
    };

    return result;
}

iff.clearAll = function () {
    iff.tests = {};
    iff.reset = null;
    iff.injectParameter = null;
};

function compileDescribe(describeObj) {
    return function () {
        if (describeObj.conditionSetup) {
            beforeAll(describeObj.conditionSetup);
        }
        if (describeObj.describes && Object.keys(describeObj.describes).length !== 0) {
            Object.entries(describeObj.describes).forEach(([describeName, innerDescribeObj]) => {
                describe(describeName, compileDescribe(innerDescribeObj));
            });
        }

        if (describeObj.tests && describeObj.tests.length !== 0) {
            describeObj.tests.forEach(testMethod => {
                testMethod();
            });
        }
    }
}

iff.startTests = function () {
    const currentDescribes = iff.tests.describes;

    const currentDescribesEntries = Object.entries(currentDescribes);
    currentDescribesEntries.sort(([describeNameFirst], [describeNameOther]) => {
        return describeNameFirst.localeCompare(describeNameOther);
    });

    if (iff.reset) {
        beforeAll(iff.reset);
    }
    Object.entries(currentDescribes).forEach(([describeName, innerDescribeObj]) => {
        describe(describeName, compileDescribe(innerDescribeObj));
    });

    return currentDescribes;
};
