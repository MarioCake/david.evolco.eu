import Vuex from "vuex";
import Vue from "vue";
import Vuetify from "vuetify";
import {EventCallerMixin} from "../src/mixins/EventCallerMixin";
import './mutationObserverMock'

Vue.use(Vuex);
Vue.use(Vuetify);
Vue.mixin(EventCallerMixin);