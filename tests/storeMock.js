function wrapMockMethod(method) {
    return jest.fn(method);
}

function mockMethods(...methods) {
    return methods.reduce((obj, method) => {
        if (typeof method === 'function' && method.name) {
            obj[method.name] = wrapMockMethod(method);
        } else if (typeof method === 'object') {
            Object.entries(method).forEach((entry) => obj[entry[0]] = wrapMockMethod(entry[1]));
        } else {
            obj[method] = jest.fn();
        }

        return obj;
    }, {});
}

export default function getStoreConfig() {
    const state = {
        projects: {
            deleteProjectDialogVisible: false,
            editProjectDialogVisible: false,
            selectedProject: null,
            projectsData: [],
            loadingProjects: false,
        },
        sprints: {
            addSprintDialogVisible: false,
            deleteSprintDialogVisible: false,
            editSprintDialogVisible: false,
            selectedSprint: null,
        },
        tasks: {
            addTaskDialogVisible: false,
            deleteTaskDialogVisible: false,
            selectedTask: null,
        }
    };

    const actions = mockMethods(
        "deleteProject",
        "deleteSprint",
        "deleteTask",
        "addSprint",
        "openDeleteProjectDialog",
        "openDeleteSprintDialog",
        "openAddSprintDialog",
        "openEditProjectDialog",
        "openAddTaskDialog",
        "openDeleteTaskDialog",
        "openEditSprintDialog",
        "saveProject",
        "saveSprint",
        "changeSprintOrder",
        "addTask",
        "changeTaskState",
        "prepareChangeSprintProject",
        "changeSprintProject"
    );
    const getters = mockMethods(
        'lastEndDate',
        'lastEndDateFormatted',
        'projectAddPossible',
        function loadingProjects(state) {
            return state.projects.loadingProjects;
        },
        function projects(state) {
            return state.projects.projectsData
        }
    );
    return {
        actions,
        getters,
        state,
    };
}
